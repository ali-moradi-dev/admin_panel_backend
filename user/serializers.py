from rest_framework import serializers
from .models import CustomUser


class CustomUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomUser
        fields = "__all__"

    def save(self):
        user = CustomUser(
            username=self.validated_data['username'],
            password=self.validated_data['password'],
            email=self.validated_data['email'],
            first_name=self.validated_data['first_name'],
            last_name=self.validated_data['last_name'],
            middle_name=self.validated_data['middle_name'],
            address_line_1=self.validated_data['address_line_1'],
            address_line_2=self.validated_data['address_line_2'],
            city=self.validated_data['city'],
            state=self.validated_data['state'],
            country=self.validated_data['country'],
            phone=self.validated_data['phone'],
            postal_code=self.validated_data['postal_code'],
            date_birth=self.validated_data['date_birth'],
            annual_income=self.validated_data['annual_income'],
            liquid_net=self.validated_data['liquid_net'],
            question1=self.validated_data['question1'],
            question2=self.validated_data['question2'],
            question3=self.validated_data['question3'],

          )
        user.set_password(self.validated_data['password'])
        user.save()



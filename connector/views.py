from django.shortcuts import render
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from connector.operations import get_initial_data


# Create your views here.
class GetInitialData(APIView):
    """ list all symbols """

    @staticmethod
    def get(request):
        get_initial_data()
        response = {
            "status": "succeed"
        }
        return Response(response, status=status.HTTP_200_OK)

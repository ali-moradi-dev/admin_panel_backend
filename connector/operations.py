import ccxt
from pandas import DataFrame
from sqlalchemy import create_engine
from common.env import *
from common.symbols import spot_symbols
import datetime


def public_api_initial(market_type):
    """
    connect to binance public api (do not need authentication) through proxy
    """

    exchange = ccxt.binance({'options': {
        'defaultType': market_type
    },
        'enableRateLimit': True})

    return exchange


def fetch_historical_ohclv(symbol, market_type, limit):
    """
    this function is written for get fetch historical data
    :param symbol: a ticker like BTC/USDT
    :param market_type: 'spot' or 'future'
    :param limit: limit 1 to 500
    :return: data list
    """
    exchange = public_api_initial(market_type)

    try:
        data = exchange.fetch_ohlcv(symbol, '1d', limit=limit)
    except Exception as e:
        data = {
            'error_type': type(e).__name__,
            'error_text': str(e)
        }

    return data


def timestamp_changer(ts):
    date = datetime.datetime.fromtimestamp((ts // 1000))
    return str(date.year) + "-" + str(date.month) + "-" + str(date.day)


def get_initial_data():
    engine = create_engine(f'postgresql://{DB_USER}:{DB_PASSWORD}@{DB_HOST}:{DB_PORT}/{DB_NAME}')
    total_data_frame = 0
    for idx, symbol in enumerate(spot_symbols):
        last_500_candles = fetch_historical_ohclv(symbol=symbol, market_type='spot', limit=500)
        candles_data_frame = DataFrame(last_500_candles, columns=['date', 'open', 'high', 'low', 'close', 'volume'])
        candles_data_frame['market'] = 'spot'
        candles_data_frame['symbol'] = symbol
        candles_data_frame['date'] = [timestamp_changer(item) for item in candles_data_frame['date']]
        if idx == 0:
            total_data_frame = candles_data_frame
        else:
            total_data_frame = total_data_frame.append(candles_data_frame)

    df = total_data_frame.iloc[1:, :]
    df = df[['symbol', 'market', 'open', 'high', 'low', 'close', 'volume', 'date']]
    df = df.reset_index()
    df['id'] = df['index']
    df.pop('index')
    df.to_sql('daily_crypto', engine, if_exists='replace', index=True)



from rest_framework import permissions


class ManagerPermission(permissions.BasePermission):
    """
	Global permission check for manager
	"""

    def has_permission(self, request, view):
        user_groups = [i.name for i in request.user.groups.all()]

        if 'admin' in user_groups:
            return True
        else:
            return False

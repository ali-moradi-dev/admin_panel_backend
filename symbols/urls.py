from .views import SymbolsListApi, SymbolHistoryApi
from django.urls import path

urlpatterns = [
    path("symbols_list/", SymbolsListApi.as_view()),
    path("history/", SymbolHistoryApi.as_view()),
]

# admin panel backend

this project provide api for get historical data .

## Installation
get package by cloning from git

```bash
git clone git@gitlab.com:ali-moradi-dev/admin_panel_backend.git
```

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install foobar.

```bash
pip install -r requirements.txt
```

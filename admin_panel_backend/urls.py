"""admin_panel_backend URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.urls import path, include
from user.views import Token, TokenValidation
from connector.views import GetInitialData
from historical_data.views import TestTask


urlpatterns = [
    path('admin/', admin.site.urls),

    path('api/v1/token/', Token.as_view()),
    path('api/v1/token/check/', TokenValidation.as_view()),
    path('api/v1/symbol/', include('symbols.urls')),
    path('api/v1/user/', include('user.urls')),
    path('api/v1/start/', GetInitialData.as_view()),
    path('api/v1/test/task/', TestTask.as_view()),
]

urlpatterns += staticfiles_urlpatterns()

import datetime

from celery import shared_task
from common.symbols import spot_symbols
from connector.operations import fetch_historical_ohclv
from historical_data.crypto_daily_model import DailyCrypto


@shared_task(queue='spot_daily_update')
def spot_daily_update() -> None:
    for ticker in spot_symbols:
        try:

            last_day_symbol_info = fetch_historical_ohclv(market_type='spot', limit=2, symbol=ticker)

            date = datetime.datetime.fromtimestamp((last_day_symbol_info[0][0] // 1000))
            date = str(date.year) + "-" + str(date.month) + "-" + str(date.day)

            action = DailyCrypto()
            action.market = "spot"
            action.symbol = ticker
            action.open = last_day_symbol_info[0][1]
            action.high = last_day_symbol_info[0][2]
            action.low = last_day_symbol_info[0][3]
            action.close = last_day_symbol_info[0][4]
            action.volume = last_day_symbol_info[0][5]
            action.date = date
            action.save()
            print(f'{ticker}, succeed')
        except Exception as e:
            print('err :', ticker, str(e))
